FROM ubuntu:14.04
MAINTAINER James Turnbull "james@example.com"
ENV REFRESHED_AT 2015-02-26
ENV http_proxy "http://10.40.30.35:3128"
ENV https_proxy "http://10.40.30.35:3128"
RUN apt-get update
RUN apt-get -y install ruby rake
RUN gem install --no-rdoc --no-ri rspec ci_reporter_rspec
